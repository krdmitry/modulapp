import QtQuick 2.14
import QtQuick.Controls 2.14

import modulapp.backend 1.0

ApplicationWindow {
    visible: true
    width: 480
    height: 640
    title: qsTr("Modulbank API tool")

    SwipeView {
        id: swipeView
        anchors.fill: parent
        currentIndex: tabBar.currentIndex

        Page1Form {
            button_connect {
                onClicked: backend.api_connect(textInputAPIAddress.text, textInputToken.text);
            }

            listView {
                model: companies

                delegate: Row {
                    Text {
                        text: name
                        width: parent.parent.width / 3
                        font.bold: true
                    }
                    Text {
                        text: id
                        width: parent.parent.width / 3
                    }
                    ListView {          //??? How to show the JSON here??
                        model: aux_data
                        delegate: Text {

                        }
                    }
                }
            }
            comboBoxCompanies {
                model: companies
                textRole: "name"
                valueRole: "id"
                onActivated: { backend.current_company = comboBoxCompanies.currentValue; }      // Set company ID as currently selected indicator
            }
        }

        Page2Form {
        }
    }

    footer: TabBar {
        id: tabBar
        currentIndex: swipeView.currentIndex

        TabButton {
            text: qsTr("Main page")
        }
        TabButton {
            text: qsTr("Account data")
        }
    }

}
