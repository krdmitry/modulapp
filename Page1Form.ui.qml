import QtQuick 2.14
import QtQuick.Controls 2.14
import Qt.labs.qmlmodels 1.0

Page {
    id: page_main
    property alias button_connect: button_connect
    property alias textInputAPIAddress: textInputAPIAddress
    property alias textLog: textLog
    property alias listView: listView
    property alias comboBoxCompanies: comboBoxCompanies
    property alias textInputAccountID: textInputAccountID
    property alias textInputToken: textInputToken
    property alias busy_indicator: busy_indicator

    title: ""

    header: Label {
        text: qsTr("Main page")
        font.pixelSize: Qt.application.font.pixelSize * 2
        padding: 10
    }

    BusyIndicator {
        id: busy_indicator
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.verticalCenter: listView.verticalCenter
        enabled: true
        running: backend.isbusy
    }

    TextInput {
        id: textInputAPIAddress
        x: 13
        y: 6
        width: 467
        height: 20
        text: qsTr("https://api.modulbank.ru")
        transformOrigin: Item.Left
        overwriteMode: false
        font.pixelSize: 12
    }

    TextInput {
        id: textInputToken
        x: 13
        y: 26
        width: 467
        height: 20
        text: qsTr("sandboxtoken")
        overwriteMode: false
        transformOrigin: Item.Left
        font.pixelSize: 12
    }

    TextInput {
        id: textInputAccountID
        x: 13
        y: 47
        width: 467
        height: 20
        text: qsTr("Bank account ID...")
        overwriteMode: false
        transformOrigin: Item.Left
        font.pixelSize: 12
    }

    Text {
        id: textLog
        y: 222
        height: 125
        text: qsTr("Program log")
        anchors.right: parent.right
        anchors.rightMargin: 5
        anchors.left: parent.left
        anchors.leftMargin: 5
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 6
        font.pixelSize: 12
    }

    Button {
        id: button_connect
        x: 350
        y: 73
        width: 100
        height: 37
        text: qsTr("Connect")
        anchors.top: textInputAccountID.bottom
        anchors.topMargin: 6
    }

    ComboBox {
        id: comboBoxCompanies
        x: 13
        width: 326
        height: 37
        anchors.top: textInputAccountID.bottom
        anchors.topMargin: 6
    }

    ListView {
        id: listView
        anchors.rightMargin: 6
        anchors.leftMargin: 6
        anchors.top: comboBoxCompanies.bottom
        anchors.right: parent.right
        anchors.bottom: textLog.top
        anchors.left: parent.left
        anchors.bottomMargin: 6
        anchors.topMargin: 6

        highlight: Rectangle {
            color: "#e2b27b"
            radius: 2
        }
        focus: true

        section.property: "name"
        section.criteria: ViewSection.FullString
        section.delegate: Rectangle {
            width: parent.width
            height: childrenRect.height
            color: "lightsteelblue"

            Text {
                text: name
                font.bold: true
                font.pixelSize: 18
            }
        }
    }
}

/*##^##
Designer {
    D{i:0;autoSize:true;height:640;width:480}D{i:3;anchors_x:134}D{i:8;anchors_y:80}
}
##^##*/

