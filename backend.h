#ifndef BACKEND_H
#define BACKEND_H

#include <QObject>
#include <QString>

#include <QUrl>
#include <QNetworkAccessManager>
#include <QNetworkReply>
#include <QAuthenticator>
#include <QNetworkProxy>
#include <QHttpMultiPart>

#include <QJsonArray>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonValue>

#include "datamodel.h"

#include <QStringList>

class Backend : public QObject
{
    Q_OBJECT

public:
    explicit Backend(QObject *parent = nullptr);

    Q_INVOKABLE QString api_connect(QString api_url, QString token = "sandboxtoken");

    Q_PROPERTY(bool isbusy READ isbusy WRITE set_isbusy NOTIFY isbusy_changed)
    bool isbusy() { return m_isbusy; }
    void set_isbusy(const bool a) { if (m_isbusy != a) { m_isbusy = a; emit isbusy_changed(); } }

    Company company;            // Companies data

    Q_PROPERTY(QString current_company READ current_company WRITE set_current_company NOTIFY current_company_changed)
    QString current_company() { return m_current_company; }
    void set_current_company(QString &a) { m_current_company = a; emit current_company_changed(); }

    /*Q_PROPERTY(QList<Company> companies READ companies WRITE set_companies NOTIFY companies_changed)
    QList<Company> companies() { return m_companies; }
    void set_companies(QList<Company> &a) { emit companies_changed(); }*/

signals:
    void isbusy_changed();
    void current_company_changed();

private slots:
    // slots for QNetworkAccessManager signals processing
    void	slot_nm_auth_required(QNetworkReply *reply, QAuthenticator *authenticator);
    void	slot_nm_encrypted(QNetworkReply *reply);
    void	slot_nm_finished(QNetworkReply *reply);
    void	slot_nm_network_accessible_changed(QNetworkAccessManager::NetworkAccessibility accessible);
    void	slot_nm_presharedkey_auth_required(QNetworkReply *reply, QSslPreSharedKeyAuthenticator *authenticator);
    void	slot_nm_proxy_auth_required(const QNetworkProxy &proxy, QAuthenticator *authenticator);
#ifndef QT_NO_SSL
    void    slot_nm_sslerrors(QNetworkReply *, const QList<QSslError> &errors);
#endif
    void	slot_reply_finished();

private:
    QUrl        m_api_url;          // URL for the api entry
    QString     m_api_token,        // authorization token
                m_api_account;      // account id (if known, otherwise accounts retrievment operation will be performed

    bool        m_isbusy = false;  // Busy indicator

    QString     m_current_company;  // ID of currently selected company;

    QNetworkAccessManager nm;       // NetworkAcessManager instance

    QString api_parse_account_info(QByteArray &data);

};

#endif // BACKEND_H
