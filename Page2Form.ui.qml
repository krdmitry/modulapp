import QtQuick 2.14
import QtQuick.Controls 2.14

Page {
    id: page_data
    width: 600
    height: 400

    header: Label {
        text: qsTr("Page 2")
        font.pixelSize: Qt.application.font.pixelSize * 2
        padding: 10
    }
}
