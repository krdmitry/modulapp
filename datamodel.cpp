#include <QDebug>       //!!

#include "datamodel.h"

Company::Company(QObject *parent): QAbstractListModel(parent)
{
    const company_t t = {.name = "vava", .id = "f8974r0987iosufd", .doc = QJsonObject{{"key", "val"}, {"key2", "val2"}} };
    m_c.append(t);
    const company_t t2 = {.name = "bobas", .id = "gfds908v09whvkl", .doc = QJsonObject{{"key3", "val3"}, {"key4", "val5"}} };
    m_c.append(t2);
    /*m_doc["INN"] = "7804289850";
    m_doc["KPP"] = "7804250";
    m_doc["bank-account"] = "sdafajstio98749879gjhlkchxk4289850";
    m_doc["OGRN"] = "6564654654250";*/
}

int Company::rowCount(const QModelIndex &) const
{
    return m_c.count();
}

QVariant Company::data(const QModelIndex &index, int role) const
{
    int row = index.row();
    if (row < rowCount()) {
        switch (role) {
        case name_role: return QVariant(m_c[row].name);
        case id_role  : return QVariant(m_c[row].id);
        case aux_role: return QVariant(m_c[row].doc);
        case accounts_role: return QVariant(m_c[row].doc["bankAccounts"].toVariant());
        default: return QVariant();
        }
    }
    return QVariant();
}

QHash<int, QByteArray> Company::roleNames() const
{
    static const QHash<int, QByteArray> roles {
        { name_role, "name" },
        { id_role, "id" },
        { aux_role, "aux_data", },
        { accounts_role, "accounts" }
    };
    return roles;
}

QVariantMap Company::get(int row) const
{
    return { {"name", QVariant(m_c[row].name)},
             {"id", QVariant(m_c[row].id)},
             {"aux_data", QVariant(m_c[row].doc)},
             {"accounts", m_c[row].doc["bankAccounts"].toVariant()} };
}

QVariantMap Company::get(QString &id) const
{
    for (int row = 0; row < m_c.count(); row++) {
        if (m_c[row].id == id) {
            return { {"name", QVariant(m_c[row].name)},
                     {"id", QVariant(m_c[row].id)},
                     {"aux_data", QVariant(m_c[row].doc)},
                     {"accounts", m_c[row].doc["bankAccounts"].toVariant()} };
        }
    }
    return (QVariantMap());
}

void Company::append(const QJsonObject &d)
{
    int row = m_c.count();
    emit beginInsertRows(QModelIndex(), row, row);
    const company_t t = {.name = d["companyName"].toString(), .id = d["companyId"].toString(), .doc = d};
    m_c.append(t);
    emit endInsertRows();
}

void Company::remove(int row)
{
    if (row < 0 || row >= m_c.count())
        return;

    beginRemoveRows(QModelIndex(), row, row);
    m_c.removeAt(row);
    endRemoveRows();
}

void Company::clear(void)
{
    m_c.clear();
}
