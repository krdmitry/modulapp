#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QQmlContext>

#include "backend.h"
#include "datamodel.h"

int main(int argc, char *argv[])
{
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
    QGuiApplication app(argc, argv);
    QQmlApplicationEngine engine;

    Backend be;
    /*// implement more clever model clearing procedure
    for (int i = be.accounts.rowCount(); i >= 0; i--) {
        be.accounts.remove(i);
    }*/

    qmlRegisterUncreatableType<Backend>("modulapp.backend", 1, 0, "Backend", "");
    qmlRegisterUncreatableType<Company>("modulapp.backend", 1, 0, "Company", "");

    engine.rootContext()->setContextProperty("backend", &be);
    engine.rootContext()->setContextProperty("companies", &be.company);

    const QUrl url(QStringLiteral("qrc:/main.qml"));
    QObject::connect(&engine, &QQmlApplicationEngine::objectCreated,
                     &app, [url](QObject *obj, const QUrl &objUrl) {
        if (!obj && url == objUrl)
            QCoreApplication::exit(-1);
    }, Qt::QueuedConnection);
    engine.load(url);

    return app.exec();
}
