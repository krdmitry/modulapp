
#include <QtCore/qdebug.h>

#include "backend.h"

/**

 * @brief Backend::Backend  Class constructor
 * @param parent
 */
Backend::Backend(QObject *parent) : QObject(parent)
{
    connect(&nm, &QNetworkAccessManager::finished,
                    this, &Backend::slot_nm_finished);
    connect(&nm, &QNetworkAccessManager::authenticationRequired,
                    this, &Backend::slot_nm_auth_required);
    connect(&nm, &QNetworkAccessManager::encrypted,
                    this, &Backend::slot_nm_encrypted);
    connect(&nm, &QNetworkAccessManager::networkAccessibleChanged,
                    this, &Backend::slot_nm_network_accessible_changed);
    connect(&nm, &QNetworkAccessManager::preSharedKeyAuthenticationRequired,
                    this, &Backend::slot_nm_presharedkey_auth_required);
    connect(&nm, &QNetworkAccessManager::proxyAuthenticationRequired,
                    this, &Backend::slot_nm_proxy_auth_required);
    #ifndef QT_NO_SSL
    connect(&nm, &QNetworkAccessManager::sslErrors,
            this, &Backend::slot_nm_sslerrors);
    #endif

    m_isbusy = false;

    qDebug() << "Backend started";
}

/**
 * @brief Backend::api_parse_account_info       Parser for 'Account info' response from bank API
 * @param data                                  Data to parse
 * @return                                      Verbose status of parsing
 */
QString Backend::api_parse_account_info(QByteArray &data)
{
    QJsonParseError json_err;
    QJsonDocument json_doc = QJsonDocument::fromJson(data, &json_err);
    if (json_doc.isArray() && json_doc.array()[0].isObject()) {
        qDebug() << "JSON array received by size: " << json_doc.array().size();
        for (int i = 0; i < json_doc.array().size(); i++) {
            qDebug() << "Object #" << i;
            QJsonObject jo = json_doc.array().at(i).toObject();
            qDebug() << "Object size :" << jo.size();
            qDebug() << "Object keys :" << jo.keys();
            qDebug() << "Company ID " << jo.value("companyId").toString() << " Name " << jo.value("companyName").toString();
            for (int j = 0; j < jo.size(); j++) {
                qDebug() << jo.keys().at(j) << " = " << jo.value(jo.keys().at(j)).toString();
            }
            //
            company.append(jo);
            //
        }
    } else {
        qDebug() << "No valid JSON data have been found, error code " << json_err.error;
    }

    return "OK";
}


QString Backend::api_connect(QString api_url, QString token)
{
    m_api_url.setUrl(api_url);
    m_api_token = token;
    m_api_account = "";

    set_isbusy(true);

    if (m_api_account.length() > 0) {
        m_api_url.setPath("/v1/operation-history/" + m_api_account);
    } else {
        m_api_url.setPath("/v1/account-info");
    }

    qDebug() << "Connect to the API at" << m_api_url.url() << " ...";

    QNetworkRequest rq(m_api_url);
    rq.setHeader(QNetworkRequest::ContentTypeHeader, QVariant("application/json"));
    QByteArray auth_data;
    auth_data.append("Bearer " + m_api_token);
    rq.setRawHeader("Authorization", auth_data);
    if (m_api_token == "sandboxtoken") {
        rq.setRawHeader("sandbox", "on");
    }

    QNetworkReply *rp_p = nm.post(rq, QByteArray(""));
    connect(rp_p, &QNetworkReply::finished, this, &Backend::slot_reply_finished);
    qDebug() << "Reply status: " << rp_p->errorString() << " " << rp_p->isRunning();

    return "OK";
}

void	Backend::slot_reply_finished()
{
    qDebug() << "Finished!!!";
}


void	Backend::slot_nm_finished(QNetworkReply *reply)
{
    set_isbusy(false);

    qDebug() << "Network manager finished: " << reply->header(QNetworkRequest::ContentLengthHeader) << " bytes received";
    if (reply->attribute(QNetworkRequest::RedirectionTargetAttribute).isNull()) {
        QByteArray read_data(reply->readAll());
        qDebug() << "Data received: " << read_data;
        if (reply->error() == QNetworkReply::NoError) {
            api_parse_account_info(read_data);
        } else {
            qDebug() << "Request error: " << reply->errorString();
        }
    } else {
        qDebug() << "API has requested redirection to: " << reply->attribute(QNetworkRequest::RedirectionTargetAttribute).toUrl();
    }
}

void	Backend::slot_nm_auth_required(QNetworkReply *reply, QAuthenticator *authenticator)
{
    (void) authenticator;
    qDebug() << "Authentification required (error: " << reply->errorString() << ", verbose: " << reply->error();
}

void	Backend::slot_nm_encrypted(QNetworkReply *reply)
{
    (void) reply;
    qDebug() << "Encrypted connection";
}

void	Backend::slot_nm_network_accessible_changed(QNetworkAccessManager::NetworkAccessibility accessible)
{
    qDebug() << "Network manager accessible changed: " << accessible;
}

void	Backend::slot_nm_presharedkey_auth_required(QNetworkReply *reply, QSslPreSharedKeyAuthenticator *authenticator)
{
    (void) authenticator;
    qDebug() << "Preshared key authentification required (error: " << reply->errorString() << ", verbose: " << reply->error();
}

void	Backend::slot_nm_proxy_auth_required(const QNetworkProxy &proxy, QAuthenticator *authenticator)
{
    (void) proxy;
    (void) authenticator;
    qDebug() << "Proxy authentification required";
}

#ifndef QT_NO_SSL
void    Backend::slot_nm_sslerrors(QNetworkReply *reply, const QList<QSslError> &errors)
{
    qDebug() << "SSL error code: " << reply->error() << ", verbose: " << reply->errorString();
    qDebug() << "SSL errors: " << errors;
}
#endif

