#ifndef DATAMODEL_H
#define DATAMODEL_H

#include <QObject>
#include <QAbstractListModel>
#include <QList>

#include <QJsonArray>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonValue>

//!! Is it worthy to define a namespace here like '_NAMESPACE_DATAMODEL'?

/**
 * @brief The Company class     To define Company datamodel, including Company name, ID, accounts info and another data.
 */
class Company : public QAbstractListModel
{
    Q_OBJECT
public:
    Company(QObject *parent = nullptr);

    enum roles_t {
        name_role = Qt::DisplayRole,
        id_role,
        aux_role,
        accounts_role
    };
    Q_ENUM(roles_t)

    int rowCount(const QModelIndex & = QModelIndex()) const;
    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const;
    QHash<int, QByteArray> roleNames() const;

    Q_INVOKABLE QVariantMap get(int row) const;
    Q_INVOKABLE QVariantMap get(QString &id) const;
    Q_INVOKABLE void remove(int row);
    Q_INVOKABLE void clear(void);
    Q_INVOKABLE void append(const QJsonObject &d);

private:
    typedef struct {
        QString name, id;           // Сompany name & ID
        QJsonObject doc;            // Auxiliary parameters
    } company_t;

    QList<company_t> m_c;
};


#endif // DATAMODEL_H
